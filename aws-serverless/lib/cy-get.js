const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({ region: "ap-southeast-1", apiVersion: '2012-08-10' });

exports.handler = async function (event) {  
  const promise = new Promise(function(resolve, reject) {
	  const type = event.type;	  
	  if(type == "all") { 
	  	const params = { TableName: "cy-data" }; 
	    dynamodb.scan(params, function(e, data) {
        const items = data.Items.map((dataField) => {
                      return { age: parseInt(dataField.age.N, 10), 
                               height: parseInt(dataField.height_cm.N, 10), 
                               income: parseInt(dataField.income_day.N, 10)};
        });
        resolve(items);
      }).on('error', (e) => {
        console.log(e);
        reject(Error(e));
      });
	  } else if(type === 'single') {
		  const params = {
		    Key: { "UserId": { S: event.userId } },
		    TableName: "cy-data"
		  };
	    dynamodb.getItem(params, function(e, data) {
        console.log("getItem data:" + JSON.stringify(data));
        resolve({  age: parseInt(data.Item.age.N, 10), 
                   height: parseInt(data.Item.height_cm.N, 10), 
                   income: parseInt(data.Item.income_day.N, 10) });      
	    }).on('error', (e) => {
        console.log(e);
        reject(Error(e));
      });
	  } else {
	  	 reject("Something went wrong.");
	  }
  });
  return promise;
};


// PREVIOUSLY WITH CALLBACKS AND ALTERNATE AUTH

// const AWS = require('aws-sdk');
// const dynamodb = new AWS.DynamoDB({ region: "ap-southeast-1", apiVersion: '2012-08-10' });
// const cisp = new AWS.CognitoIdentityServiceProvider({apiVersion: '2016-04-18'});

// exports.handler = (event, context, callback) => {
//   const accessToken = event.accessToken;
//   const type = event.type;
//   if(type == "all") {
 
//     const params = { TableName: "cy-data" };
// 	dynamodb.scan(params, function(err, data) {
// 	  if (err) {
// 		console.log(err);
// 		callback(err);
//   	  } else {
//   	  	console.log(JSON.stringify(data));
// 		const items = data.Items.map((dataField) => {
// 		  return {age: parseInt(dataField.age.N), height: parseInt(dataField.height_cm.N), income: parseInt(dataField.income_day.N)};
// 		});

//   	    callback(null, items);
//   	  }
//     }); 
//    } else if(type === 'single') {
//     //getItem
//      const cispParams = {
//     	"AccessToken": accessToken
//      }
//      cisp.getUser(cispParams, (err, result) => {
//      	if(err) {
// 		  console.log(err);	
//      	  callback(err);
//      	} else {
//      	  console.log(result);
//      	  const userId = result.UserAttributes[0].Value;
// 		  const params = {
// 	        Key: { "UserId": { S: userId } },
// 		    TableName: "cy-data"
// 	      };
// 		  dynamodb.getItem(params, function(err, data) {
// 			if (err) {
// 			  console.log(err);
// 			  callback(err);
// 	  	    } else {
// 	          console.log(data);  	    
// 	  	      callback(null, {age: parseInt(data.Item.age.N), height: parseInt(data.Item.height_cm.N), income: parseInt(data.Item.income_day.N)});
// 	  	    }
// 		  });
//      	}
//      });
//     } else {
//       console.log("Something went wrong.");
//     }
// };
